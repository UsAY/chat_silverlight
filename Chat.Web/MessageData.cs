﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Chat.Web
{
	public class MessageData
	{
		public ChatClient Client { get; set; }
		public string Message { get; set; }
		public DateTime Time { get; set; }
	}
}
