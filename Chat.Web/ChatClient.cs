﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chat.Web
{
	public class ChatClient
	{
		/// <summary>
		/// Gets or sets the name of the client.
		/// </summary>
		/// <value>The name of the client.</value>
		public string ClientName { get; set; }
		private List<string> messages = new List<string>();

		private IChatService client;

		private volatile bool isSending;

		/// <summary>
		/// Gets or sets a value indicating whether this instance is sending.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is sending; otherwise, <c>false</c>.
		/// </value>
		public bool IsSending
		{
			get { return isSending; }
			set { isSending = value; }
		}

		public event EventHandler Fault;

		public ChatClient(string name, IChatService clientChannel)
		{
			ClientName = name;
			client = clientChannel;
		}

		/// <summary>
		/// Sends the specified message.
		/// </summary>
		/// <param name="msg">The message.</param>
		public void Send(string msg)
		{
			try
			{
				isSending = true;
				client.BeginSendMessage(msg, new AsyncCallback(EndSend), null);
			}
			catch (Exception ex)
			{
				isSending = false;
				OnFault();
			}
		}

        public void SendPrivate(string user,string toUser, string msg)
        {
            try
            {
                isSending = true;
                client.BeginSendPrivateMessage(user,toUser,msg, new AsyncCallback(EndPrivateSend), null);
            }
            catch(Exception ex)
            {
                isSending = false;
				OnFault();
            }

        
        }
		private void EndSend(IAsyncResult result)
		{
			try
			{
				isSending = false;
				client.EndSendMessage(result);
			}
			catch
			{
				OnFault();
			}
		}

        private void EndPrivateSend(IAsyncResult result)
        {
            try
            {
                isSending = false;
                client.EndSendPrivateMessage(result);
            }
            catch(Exception ex)
            {
                OnFault();
            }
        }
		/// <summary>
		/// Users the connected.
		/// </summary>
		/// <param name="name">The name.</param>
		public void UserConnected(string name)
		{
			try
			{
				isSending = true;
				client.BeginUserConnected(name, new AsyncCallback(EndUserConnected), null);
			}
			catch
			{
				OnFault();
			}
		}

		private void EndUserConnected(IAsyncResult result)
		{
			try
			{
				isSending = false;
				client.EndUserConnected(result);
			}
			catch
			{
				OnFault();
			}
		}

		/// <summary>
		/// Users the disconnected.
		/// </summary>
		/// <param name="name">The name.</param>
		public void UserDisconnected(string name)
		{
			try
			{
				isSending = true;
				client.BeginUserDisconnected(name, new AsyncCallback(EndUserDisconnected), null);
			}
			catch
			{
				OnFault();
			}
		}

		private void EndUserDisconnected(IAsyncResult result)
		{
			try
			{
				isSending = false;
				client.EndUserDisconnected(result);
			}
			catch
			{
				OnFault();
			}
		}

		private void OnFault()
		{
			if (client != null)
			{
				var handler = Fault;
				
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
	}
}
