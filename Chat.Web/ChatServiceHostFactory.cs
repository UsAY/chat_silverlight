﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Activation;

namespace Chat.Web
{
	public class ChatServiceHostFactory : ServiceHostFactoryBase
	{
		public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
		{
			return new ChatServiceHost(baseAddresses);
		}
	}

	class ChatServiceHost : ServiceHost
	{
		public ChatServiceHost(params Uri[] addresses)
		{
			base.InitializeDescription(typeof(ChatService), new UriSchemeKeyedCollection(addresses));
		}

		protected override void InitializeRuntime()
		{
			PollingDuplexBindingElement pdbe = new PollingDuplexBindingElement()
			{
				ServerPollTimeout = TimeSpan.FromSeconds(3),
				//Duration to wait before the channel is closed due to inactivity
				InactivityTimeout = TimeSpan.FromMinutes(10)
			};

			this.AddServiceEndpoint(typeof(ChatService),
				new CustomBinding(
					pdbe,
					new BinaryMessageEncodingBindingElement(),
					new HttpTransportBindingElement()), string.Empty);

			base.InitializeRuntime();
		}
	}
}
