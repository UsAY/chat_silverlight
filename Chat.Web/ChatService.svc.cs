﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Chat.Web
{
	[ServiceContract(Namespace = "", CallbackContract = typeof(IChatService))]
	public class ChatService
	{
		private static Dictionary<string, ChatClient> Users = new Dictionary<string,ChatClient>();
		

		/// <summary>
		/// Connects the specified user name.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns>is connected</returns>
		[OperationContract]
		public bool Connect(string userName)
		{
			var result = false;
			if (!Users.Keys.Contains(userName))
			{
				var clientChannel = OperationContext.Current.GetCallbackChannel<IChatService>();
				var chatClient = new ChatClient(userName, clientChannel);
				chatClient.Fault += new EventHandler(chatClient_Fault);
				Users.Add(userName, chatClient);
				NotifyUsers(userName, true);
				result = true;
			}
			return result;
		}

		/// <summary>
		/// Gets the connected users.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		public string[] GetConnectedUsers()
		{
			return Users.Keys.ToArray();
		}

		void chatClient_Fault(object sender, EventArgs e)
		{
			var clientOffline = (ChatClient)sender;
			Users.Remove(clientOffline.ClientName);
			NotifyUsers(clientOffline.ClientName, false);
		}

		/// <summary>
		/// Disconnects the specified user name.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		[OperationContract(IsOneWay=true)]
		public void Disconnect(string userName)
		{
			if (Users.Keys.Contains(userName))
			{
				var client = Users[userName];
                NotifyUsers(userName, false);
				Users.Remove(userName);
				client.UserDisconnected(userName);
			}
		}
        /// <summary>
        /// уведомляет пользователей о событиях в чате
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="isConnected"></param>
		private void NotifyUsers(string userName, bool isConnected)
		{
			List<string> offlineUsers = new List<string>();
            if (isConnected) Send(userName, "logs in to chat");
            else Send(userName, "is logged out of the chat");
			for (int i = 0; i < Users.Count; i++)
			{
				string key = Users.ElementAt(i).Key;
				if (isConnected)
				{
					Users[key].UserConnected(userName);
                    
                   
				}
				else
				{
					Users[key].UserDisconnected(userName);
                   
				}
			}
		}

		/// <summary>
		/// Sends the specified user name.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="msg">The message of the user.</param>
		[OperationContract(IsOneWay = true)]
		public void Send(string userName, string msg)
		{
			if (Users.Keys.Contains(userName))
			{
				var messageData = new MessageData()
				{
					Client = Users[userName],
					Message=msg,
					Time = DateTime.Now
				};
				
				foreach (var user in Users)
				{
					user.Value.Send(string.Format("{0} {1}: {2}", messageData.Time.ToString(), userName, msg));
				}
			}
		}
        [OperationContract(IsOneWay = true)]
        
        public void SendPrivate(string userName,string toUser, string msg)
        { 
        if (Users.Keys.Contains(userName))
			{
				var messageData = new MessageData()
				{
					Client = Users[userName],
					Message=msg,
					Time = DateTime.Now
				};
				
               var us=Users.FirstOrDefault(name=>name.Value.ClientName==toUser);
               
                us.Value.SendPrivate(toUser, userName, userName+": "+msg);
            	
			}
		}
        }
	

	/// <summary>
	/// Callback interface
	/// </summary>
	[ServiceContract]
	public interface IChatService
	{
		[OperationContract(IsOneWay = true, AsyncPattern=true)]
		IAsyncResult BeginUserConnected(string UserName, AsyncCallback callback, object state);

		void EndUserConnected(IAsyncResult result);

		[OperationContract(IsOneWay = true, AsyncPattern=true)]
		IAsyncResult BeginUserDisconnected(string UserName, AsyncCallback callback, object state);

		void EndUserDisconnected(IAsyncResult result);

		[OperationContract(IsOneWay = true, AsyncPattern=true)]
		IAsyncResult BeginSendMessage(string Message, AsyncCallback callback, object state);
        [OperationContract(IsOneWay = true, AsyncPattern = true)]
        IAsyncResult BeginSendPrivateMessage(string User,string toUser,string Message, AsyncCallback callback, object state);
		void EndSendMessage(IAsyncResult result);
        void EndSendPrivateMessage(IAsyncResult result);
	}
}

