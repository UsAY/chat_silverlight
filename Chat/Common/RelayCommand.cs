﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Chat.Common
{
    public class RelayCommand:ICommand
    {
         private Func<bool> canExecute;
        private Action executeAction;
        public event EventHandler CanExecuteChanged; 

        public RelayCommand(Action executeAction,
            Func<bool> canExecute)
        {
            this.executeAction = executeAction;
            this.canExecute = canExecute;
        }

        public RelayCommand(Action executeAction)
        {
           this.executeAction = executeAction;
            this.canExecute = () => true;
        }

        public void RaiseCanExecuteChanged()
       {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }

        public bool CanExecute(object parameter)
         {
           return canExecute == null ? true : canExecute();
        }

        public void Execute(object parameter)
        {
            executeAction();
        }
    }
 }

