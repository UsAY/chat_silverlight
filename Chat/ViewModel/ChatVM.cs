﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows.Input;
using Chat.ChatService;
using Chat.Common;
using System.Windows;
using System.Windows.Controls;

namespace Chat.ViewModel
{
	public class ChatVM :ViewModelBase
	{
		private static ChatServiceClient proxy;
		private static EndpointAddress address;
		private static CustomBinding binding;

        private ICommand openPrivateChat;
        public ICommand OpenPrivateChat
        {
            get { if (openPrivateChat == null) openPrivateChat = new RelayCommand(OpenChat);
            return openPrivateChat;
            }
        }

        private void OpenChat()
        {
            Datatemp.fromUser = SelectListBoxItem;
            Datatemp.user = UserName;
            Datatemp.proxy = proxy;
            Datatemp.Msg = " ";
            OpenNewPrivateChat();
        }
        private ChildWindow prMsgView;
        private void OpenNewPrivateChat()
        {
            
                prMsgView = new ChildWindow
               {
                   Title = "Private message " + " " + SelectListBoxItem,
                   Content = new Views.PrivateMessageView()
               };
                prMsgView.Show();
            
            
           // PrivateMessageVM prMsgVM = new PrivateMessageVM(UserName, SelectListBoxItem, proxy);
        }
		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>The users of chat.</value>
		public ObservableCollection<string> Users { get; set; }
		/// <summary>
		/// Gets or sets the messages list.
		/// </summary>
		/// <value>The messages of chat.</value>
		public ObservableCollection<string> Messages { get; set; }

		private bool isConnected;
		/// <summary>
		/// Gets or sets a value indicating whether this instance is connected.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is connected; otherwise, <c>false</c>.
		/// </value>
		public bool IsConnected 
		{
			get { return isConnected; }
			set
			{
				var isNew = isConnected != value;
				isConnected = value;
				if (isNew)
				{
					NotifyPropertyChanged("IsConnected");
				}

			}
		}

		private string userName;
		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>The name of the user.</value>
		public string UserName
		{
			get
			{
				return userName;
			}
			set
			{
				userName = value;
                NotifyPropertyChanged("UserName");
			}
		}

		private string currentMessage;

		/// <summary>
		/// Gets or sets the current message.
		/// </summary>
		/// <value>The current message.</value>
		public string CurrentMessage
		{
			get { return currentMessage; }
			set
			{
				currentMessage = value;
                NotifyPropertyChanged("CurrentMessage");
			}
		}
       
     

        private string selectListBoxItem;
        public string SelectListBoxItem
        {
            get { return selectListBoxItem; }
            set { selectListBoxItem = value;
            NotifyPropertyChanged("SelectListBoxItem");
            }
        }

		static ChatVM()
		{
			address = new EndpointAddress("http://localhost:63488/ChatService.svc");
			binding = new CustomBinding(
				new PollingDuplexBindingElement(), 
				new BinaryMessageEncodingBindingElement(),
				new HttpTransportBindingElement()
			);
			proxy = new ChatServiceClient(binding, address);
		}

		public ChatVM()
		{
			Users = new ObservableCollection<string>();
			Messages = new ObservableCollection<string>();

			proxy.ConnectCompleted += new EventHandler<ConnectCompletedEventArgs>(proxy_ConnectCompleted);
		}

		void proxy_UserDisconnectedReceived(object sender, UserDisconnectedReceivedEventArgs e)
		{
			if (Users.Contains(e.UserName))
			{
				
				Users.Remove(e.UserName);
			}
		}

		void proxy_UserConnectedReceived(object sender, UserConnectedReceivedEventArgs e)
		{
			if (e.Error == null)
			{
				//Add new connected user
				AddUser(e.UserName);
			}
		}

		void proxy_ConnectCompleted(object sender, ConnectCompletedEventArgs e)
		{
			if (e.Error == null)
			{
				if (e.Result)
				{
					IsConnected = true;
					proxy.UserConnectedReceived += new EventHandler<UserConnectedReceivedEventArgs>(proxy_UserConnectedReceived);
					proxy.UserDisconnectedReceived += new EventHandler<UserDisconnectedReceivedEventArgs>(proxy_UserDisconnectedReceived);
					proxy.SendMessageReceived += new EventHandler<SendMessageReceivedEventArgs>(proxy_SendMessageReceived);
                    proxy.SendPrivateMessageReceived += new EventHandler<SendPrivateMessageReceivedEventArgs>(proxy_SendPrivateMessageReceived);
					proxy.GetConnectedUsersCompleted += new EventHandler<GetConnectedUsersCompletedEventArgs>(proxy_GetConnectedUsersCompleted);
					proxy.GetConnectedUsersAsync();
				}
			}
		}

		void proxy_SendMessageReceived(object sender, SendMessageReceivedEventArgs e)
		{
			if (e.Error == null)
			{
				Messages.Add(e.Message);
			}
		}
        ////////////////
        void proxy_SendPrivateMessageReceived(object sender, SendPrivateMessageReceivedEventArgs e)
        {
            if (prMsgView == null)
            {
                if (e.Error == null)
                {
                    Datatemp.Msg = e.Message;
                    Datatemp.user = e.User;
                    Datatemp.fromUser = e.toUser;
                    Datatemp.proxy = proxy;
                    Datatemp.Msg = e.Message;
                    OpenNewPrivateChat();
                }
            }
            else prMsgView.Show();
        }

		void proxy_GetConnectedUsersCompleted(object sender, GetConnectedUsersCompletedEventArgs e)
		{
			if (e.Error == null)
			{
				AddUsers(e.Result);
				proxy.GetConnectedUsersCompleted -= proxy_GetConnectedUsersCompleted;
			}
		}

		/// <summary>
		/// Connects this instance to server
		/// </summary>
		public void Connect()
		{
			if (!IsConnected)
			{
				proxy.ConnectAsync(UserName);
			}
		}

		/// <summary>
		/// Sends the current message of user 
		/// </summary>
		public void SendMessage()
		{
			proxy.SendAsync(UserName, CurrentMessage);
            CurrentMessage = "";
		}
        /// <summary>
		/// Disconnects this instance.
		/// </summary>
		public void Disconnect()
		{
			if (IsConnected)
			{
				proxy.DisconnectAsync(UserName);
				proxy.DisconnectCompleted += new EventHandler<AsyncCompletedEventArgs>(proxy_DisconnectCompleted);
			}
		}

		void proxy_DisconnectCompleted(object sender, AsyncCompletedEventArgs e)
		{
			IsConnected = false;
			Users.Clear();
		}

		private void AddUsers(ObservableCollection<string> users)
		{
			for (int i = 0; i < users.Count; i++)
			{
				if (!Users.Contains(users[i]))
				{
					Users.Add(users[i]);
				}
			}
		}

		private void AddUser(string userName)
		{
			if (!Users.Contains(userName))
			{
				Users.Add(userName);
			}
		}

	}
}
