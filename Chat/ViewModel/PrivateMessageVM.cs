﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Chat.ChatService;
using Chat.Common;

namespace Chat.ViewModel
{
    public class PrivateMessageVM : ViewModelBase 
    {
        //Collection to storage message
        public ObservableCollection<string> MessagesList { get; set; } 
        private static ChatServiceClient proxy;
        
        private string _fromusername;
        public string FromUserName
        {
            get { return _fromusername; }
           private set
            {
                _fromusername = value;
                NotifyPropertyChanged("FromUserName");
            }
        }
      
        private string username;
        public string UserName
        {
            get { return username; }
          private  set
            {
                username = value;
                NotifyPropertyChanged("UserName");
            }
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyPropertyChanged("Message");
            }
        }
        #region Constructors
        public PrivateMessageVM() {
            FromUserName = Datatemp.fromUser;
            UserName = Datatemp.user;
            proxy = Datatemp.proxy;
            MessagesList = new ObservableCollection<string>();
            MessagesList.Add(Datatemp.Msg);
            Init();
            
        }
       
 #endregion constructors


        private void Init()
        {
            proxy.SendPrivateMessageReceived += new EventHandler<SendPrivateMessageReceivedEventArgs>(proxy_SendPrivateMessageRecuved);
        }
        void proxy_SendPrivateMessageRecuved(object sender, SendPrivateMessageReceivedEventArgs e)
        {
            if (e.Error == null) 
            MessagesList.Add(e.Message);
        }
        

       

        private void SendPrivateMessage()
        {
            if (!string.IsNullOrEmpty(Message))
            {
                proxy.SendPrivateAsync(Datatemp.user, Datatemp.fromUser, Message);
                MessagesList.Add(Message);
                Message = "";
            }
        }
        #region ICommand
        private ICommand sendPrivateMsg;

        public ICommand SendPrivateMsg
        {
            get
            {
                if (sendPrivateMsg == null) sendPrivateMsg = new RelayCommand(SendPrivateMessage);
                return sendPrivateMsg;
            }
        }
       /* private ICommand sendMsgKeyDown;

        public ICommand SendMesPressEnter
        {
            get
            {
                if (sendMsgKeyDown == null) sendMsgKeyDown = new RelayCommand(SendPrivateMessage);
                return sendMsgKeyDown;
            }
        }*/

        #endregion Icommand

    }
}
