﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Chat.Converter
{
	public class VisibilityConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var result = Visibility.Collapsed;
			if (value is bool)
			{
				result = ((bool)value) ? Visibility.Visible : result;
			}
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	public class NoVisibilityConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var result = Visibility.Visible;
			if (value is bool)
			{
				result = ((bool)value) ? Visibility.Collapsed : result;
			}
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var result = Visibility.Collapsed;
			if (value is Visibility)
			{
				result = (Visibility)value;
			}
			return Visibility.Collapsed;
		}

		#endregion
	}
}
