﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Chat.ViewModel;
using System.Windows.Data;

namespace Chat
{
	public partial class HomePage : Page
	{
		private ChatVM chatVM;

		public HomePage()
		{
			InitializeComponent();
			chatVM = new ChatVM();
			this.Loaded += new RoutedEventHandler(HomePage_Loaded);
		}

		void HomePage_Loaded(object sender, RoutedEventArgs e)
		{
			this.DataContext = chatVM;
		}

		// Executes when the user navigates to this page.
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
		}

		private void btnConnect_Click(object sender, RoutedEventArgs e)
		{
			var tag = ((Button)sender).Tag.ToString();
			if (string.Compare(tag, "Connect") == 0)
			{
				chatVM.Connect();
			}
			else
			{
				chatVM.Disconnect();
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			chatVM.SendMessage();
            
           
		}

       
	}
}
